import os
import json
from colorama import Fore, Style

data = []

at = "1" # Get all tasks.
ct = "2" # Get completed tasks.
ut = "3" # Get uncompleted tasks.
nt = "4" # Add new task.
cu = "5" # Complete a task.
ds = "6" # Delete a task.
dt = "7" # Delete completed tasks.
pr = "8" # Progress.
ex = "0" # Exit.
menu = [['Get all tasks', at], ['Get completed tasks', ct], ['Get uncompleted tasks', ut], ['Add new task', nt], ['Complete a task', cu], ['Delete a task', ds], ['Delete completed tasks', dt], ['Progress', pr], ['Exit', ex]]
path = "../task_list.json"

# Reading data from the file
with open(path, 'r') as f:
    data = json.load(f)

# Save changes into json file.
def save():
    global data
    # Writing data to a file
    with open(path, 'w') as f:
        json.dump(data, f)

# Main menu.
def start():
    clear()
    print(Style.BRIGHT + Fore.GREEN + "[" +
              Fore.YELLOW + "*" +
              Fore.GREEN + f"] Initializing tasks")
    newline()
    for array in menu:
        print(Style.NORMAL + Fore.WHITE + "[" +
              Fore.BLUE  + array[1] +
              Fore.WHITE + f"] " + array[0])
    newline()
    userinput = input()
    out(userinput)

# Wait for any key to be pressed then return to main menu.
def restart():
    newline()
    print(Style.BRIGHT + Fore.GREEN + "[" +
              Fore.YELLOW + "*" +
              Fore.GREEN + f"] Press any key to continue")
    input()
    start()

# Jump into a new line.
def newline():
    print()

# Clear console.
def clear():
    os.system("clear")

# Add new item into the list.
def addnew(name, desc, finish, ut):
    data.append([name, desc, finish, ut])

def out(m):
    """The output function.
    """

    global data
    if m == at:
        clear()
        for array in data:
            print(Style.BRIGHT + Fore.WHITE + "[" +
                    Fore.CYAN + "+" +
                    Fore.WHITE + "]" +
                    Fore.CYAN +
                    f" {array[0]}: " +
                    Style.RESET_ALL +
                    f"{array[1]}")
        restart()

    elif m == ct:
        clear()
        for array in data:
            if array[3] == m:
                print(Style.BRIGHT + Fore.WHITE + "[" +
                    Fore.MAGENTA + "+" +
                    Fore.WHITE + "]" +
                    Fore.MAGENTA +
                    f" {array[0]}: " +
                    Style.RESET_ALL +
                    f"{array[1]}")
        restart()

    elif m == ut:
        clear()
        for array in data:
            if array[3] == m:
                print(Style.BRIGHT + Fore.WHITE + "[" +
                    Fore.RED + "+" +
                    Fore.WHITE + "]" +
                    Fore.RED +
                    f" {array[0]}: " +
                    Style.RESET_ALL +
                    f"{array[1]}")
        restart()

    elif m == nt:
        clear()
        name = input(Style.BRIGHT + Fore.WHITE + "[" +
            Fore.LIGHTYELLOW_EX + "?" +
            Fore.WHITE + "]" +
            Fore.LIGHTYELLOW_EX +
            f" Name: ")
        desc = input(Style.BRIGHT + Fore.WHITE + "[" +
            Fore.LIGHTYELLOW_EX + "?" +
            Fore.WHITE + "]" +
            Fore.LIGHTYELLOW_EX +
            f" Description: ")
        finish = input(Style.BRIGHT + Fore.WHITE + "[" +
            Fore.LIGHTYELLOW_EX + "?" +
            Fore.WHITE + "]" +
            Fore.LIGHTYELLOW_EX +
            f" End date: ")
        addnew(name, desc, finish, ut)
        save()
        restart()

    elif m == cu:
        clear()
        name = input(Style.BRIGHT + Fore.WHITE + "[" +
            Fore.LIGHTBLUE_EX + "?" +
            Fore.WHITE + "]" +
            Fore.LIGHTBLUE_EX +
            f" Name: ")
        for array in data:
            if array[0] == name:
                array[3] = ct
        save()
        start()
    
    elif m == ds:
        clear()
        name = input(Style.BRIGHT + Fore.WHITE + "[" +
            Fore.LIGHTBLACK_EX + "?" +
            Fore.WHITE + "]" +
            Fore.LIGHTBLACK_EX +
            f" Name: ")
        for array in data:
            if array[0] == name:
                data.pop(data.index(array))
        save()
        start()
    
    elif m == dt:
        for array in data:
            if array[3] == ct:
                data.pop(data.index(array))
        save()
        start()

    elif m == pr:
        clear()
        c = 0
        a = 0
        for array in data:
            if array[3] == ct:
                c += 1
            a += 1
            print(Style.NORMAL + Fore.WHITE + "[" +
                    Fore.MAGENTA + "+" +
                    Fore.WHITE + "]" +
                    Fore.MAGENTA +
                    f" {array[0]}: " +
                    Style.RESET_ALL +
                    f"{array[2]}")
        if a > 0:
            newline()
            percentage = round((c * 100 / a), 1)
            print(Style.NORMAL + Fore.WHITE + "[" +
                    Fore.RED + "/" +
                    Fore.WHITE + "]" +
                    Fore.RED +
                    f" The completed tasks percentage is: " +
                    Style.RESET_ALL +
                    f"{percentage}%")
        restart()

    elif m == ex:
        clear()
        exit(0)

    else:
        start()