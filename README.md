<div align="center">
    <img src="./images/preview.png" alt="" align="center" />
</div>

## Overview

A simple to-do list app, built using python.

## Installation

```console
# clone the repo
$ git clone https://codeberg.org/abubakr/todo-app.git

# change the working directory to todo-app
$ cd todo-app/todo

# install the requirements
$ python3 -m pip install -r requirements.txt
```

## Usage

```console
$ python3 todo.py
```

## License

see the LICENSE file.

***Uses must be at all costs under Islamic law, no exceptions.***

## Donation

Donations of all sizes are appreciated!

<div align="center">
    <img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Flogos-download.com%2Fwp-content%2Fuploads%2F2018%2F05%2FMonero_logo_colour.png&f=1&nofb=1&ipt=3aa51411bac56ed1aee446eb0b3e621c04e9fe0b5ace0ca0ea9c988aca98536a&ipo=images" alt="" align="center" width="130" height="130"/>


    86k3Sc1RtvYbAuyFbUUfTdRch23dM5tSmTxQFQeDV1TJgfkCXG9Z72YjQvevrvx36wicVADRLhYZu9dCM6AhBSTvAsi2WZz

</div>